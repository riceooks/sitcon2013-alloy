var controller = $.user;
var message = '';
var dialog = Ti.UI.createAlertDialog({
	title: '提示訊息',
	buttonNames: ['確定']
});

function update(e) {
	// 更新會員資料
	var xhr = Ti.Network.createHTTPClient({
		onload: function(e){
			response = JSON.parse(this.responseText);
			if( response.success ){
				// 更新成功
				dialog.message = '資料更新完成';
				dialog.show();
			} else {
				// 更新失敗
				message = response.data.errors.message[0];
				dialog.message = message;
				dialog.show();
			}
		},
		onerror: function(e){
			Ti.API.error( e.message );
		},
		timeout: 3000
	});

	message = '';
	var params = {};
	
	if( $.nickname.value.length == 0 ){
		message += '請輸入暱稱\n';
	} else {
		params.nickname = $.nickname.value;
	}
	
	if( $.verify.value.length == 0 || $.verify.value != $.password.value ){
		if( $.password.value.length < 6 ){
			message += '密碼必須大於6個字元\n';
		}
		
		if( $.verify.value.length != 0 ){
			message += '密碼與確認密碼不相同\n';
		}
		
		params.password = $.password.value;
	}
	
	if(message == ''){
		
	}
	
	xhr.open('POST', 'http://www.barryblogs.com/index.php?feed=sitcon&action=update');
	xhr.send(params);
}

function back(){
	controller.close();
}

$.nickname.value = Alloy.Globals.user.nickname;
$.account.text = Alloy.Globals.user.account;
controller.open();