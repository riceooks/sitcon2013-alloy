var controller = $.login;
function login(e) {
	$.message.setVisible( false );
	
	// 登入
	var xhr = Ti.Network.createHTTPClient({
		onload: function(e){
			response = JSON.parse(this.responseText);
			if( response.success ){
				// 登入成功
				Alloy.Globals.user.account = response.data.account;
				Alloy.Globals.user.nickname = response.data.nickname;

				Ti.API.debug( Alloy.Globals.user.account );
				Ti.API.debug( Alloy.Globals.user.nickname );
				Alloy.Globals.openMainWindow('user');
			} else {
				$.message.color = 'red';
				$.message.text = response.data.errors.message[0];
				$.message.setVisible( true );
			}
		},
		onerror: function(e){
			Ti.API.error( e.message );
		},
		timeout: 3000
	});
	
	var params = {
			'account': $.account.value,
			'password': Ti.Utils.md5HexDigest($.password.value)
	};
	
	xhr.open('POST', 'http://www.barryblogs.com/index.php?feed=sitcon&action=login');
	xhr.send(params);
}

function register(e){
	Alloy.Globals.openMainWindow('register');
}

$.login.open();