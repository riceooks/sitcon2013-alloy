var controller = $.register;
var message = '';
var dialog = Ti.UI.createAlertDialog({
	title: '提示訊息',
	buttonNames: ['確定']
});

function submit(){
	
	message = '';
	
	if( $.nickname.value.length == 0 ){
		message += '請輸入暱稱\n';
	}
	
	if( $.account.value.length < 6 ){
		message += '請輸入帳號\n';
	}
	
	if( $.password.value.length < 6 ){
		message += '密碼必須大於6個字元\n';
	}
	
	if( $.verify.value.length == 0 || $.verify.value != $.password.value ){
		
		if( $.verify.value.length != 0 ){
			message += '密碼與確認密碼不相同\n';
		} else {
			message += '請輸入確認密碼\n';
		}
	}
	
	if(message == ''){
		// 註冊
		var xhr = Ti.Network.createHTTPClient({
			onload: function(){
				response = JSON.parse(this.responseText);
				if(response.success){
					// 註冊成功
					Alloy.Globals.user.account = response.data.account;
					Alloy.Globals.user.nickname = response.data.nickname;
					
					Alloy.Globals.openMainWindow('user');
					controller.close();
				}
				
				// 註冊失敗
				message = response.data.errors.message[0];
				dialog.message = message;
				dialog.show();
			},
			onerror: function(e){
				// 連線錯誤
				message = '請檢查網路連線';
				dialog.message = message;
				dialog.show();
			},
			timeout: 8000
		});
		var params = {
			account: $.account.value,
			password: $.password.value,
			nickname: $.nickname.value
		};
		xhr.open('POST', 'http://www.barryblogs.com/index.php?feed=sitcon&action=register');
		xhr.send(params);
	} else {
		dialog.message = message;
		dialog.show();
	}
}

function back(){
	controller.close();
}

controller.open();