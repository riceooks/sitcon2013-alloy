var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Globals.openMainWindow = function(window) {
    Alloy.createController(window).getView().open();
};

Alloy.Globals.user = {
    account: "",
    nickname: ""
};

Alloy.createController("index");