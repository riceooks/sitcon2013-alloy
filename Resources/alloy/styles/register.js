module.exports = [ {
    isApi: true,
    priority: 1000.002,
    key: "Label",
    style: {
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000"
    }
}, {
    isClass: true,
    priority: 10000.0016,
    key: "container",
    style: {
        backgroundColor: "#4A4A4A"
    }
}, {
    isClass: true,
    priority: 10000.0019,
    key: "header_tools",
    style: {
        width: "69%",
        backgroundColor: "#EBEBEA"
    }
}, {
    isClass: true,
    priority: 10000.0024,
    key: "field_row",
    style: {
        height: Ti.UI.SIZE,
        layout: "horizontal"
    }
}, {
    isClass: true,
    priority: 10000.0025,
    key: "register_label",
    style: {
        width: "25%",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        height: Ti.UI.SIZE,
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false
    }
}, {
    isClass: true,
    priority: 10000.0026,
    key: "register_input",
    style: {
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE
    }
}, {
    isId: true,
    priority: 100000.0017,
    key: "home",
    style: {
        width: "30%",
        height: "100%",
        backgroundColor: "#EBEBEA",
        backgroundSelectedColor: "#F5C55E",
        right: "1%"
    }
}, {
    isId: true,
    priority: 100000.0018,
    key: "title",
    style: {
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "48px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    }
}, {
    isId: true,
    priority: 100000.0021,
    key: "header",
    style: {
        height: "10%",
        top: 0,
        backgroundColor: "black",
        layout: "horizontal"
    }
}, {
    isId: true,
    priority: 100000.0022,
    key: "content",
    style: {
        height: Ti.UI.SIZE,
        top: "10%",
        bottom: "10%",
        layout: "vertical"
    }
}, {
    isId: true,
    priority: 100000.0023,
    key: "footer",
    style: {
        height: "10%",
        bottom: 0,
        backgroundColor: "#4A4A4A"
    }
}, {
    isId: true,
    priority: 100000.0027,
    key: "copyright",
    style: {
        color: "white",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    }
} ];