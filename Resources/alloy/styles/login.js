module.exports = [ {
    isApi: true,
    priority: 1000.0006,
    key: "Label",
    style: {
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000"
    }
}, {
    isClass: true,
    priority: 10000.0004,
    key: "container",
    style: {
        backgroundColor: "#4A4A4A"
    }
}, {
    isClass: true,
    priority: 10000.0011,
    key: "field_row",
    style: {
        height: Ti.UI.SIZE,
        layout: "horizontal"
    }
}, {
    isClass: true,
    priority: 10000.0012,
    key: "login_label",
    style: {
        width: "20%",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        color: "white",
        height: Ti.UI.SIZE
    }
}, {
    isClass: true,
    priority: 10000.0013,
    key: "login_input",
    style: {
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE
    }
}, {
    isId: true,
    priority: 100000.0005,
    key: "logo",
    style: {
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE
    }
}, {
    isId: true,
    priority: 100000.0007,
    key: "header",
    style: {
        height: "20%",
        top: 0,
        backgroundColor: "#065efe"
    }
}, {
    isId: true,
    priority: 100000.0008,
    key: "content",
    style: {
        top: "20%",
        bottom: "20%",
        layout: "vertical"
    }
}, {
    isId: true,
    priority: 100000.0009,
    key: "footer",
    style: {
        height: "10%",
        bottom: 0,
        backgroundColor: "#4A4A4A"
    }
}, {
    isId: true,
    priority: 100000.001,
    key: "message",
    style: {
        color: "red",
        font: {
            fontSize: "28px"
        }
    }
}, {
    isId: true,
    priority: 100000.0014,
    key: "copyright",
    style: {
        color: "white",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    }
} ];