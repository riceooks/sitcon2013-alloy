function Controller() {
    function login() {
        $.message.setVisible(false);
        var xhr = Ti.Network.createHTTPClient({
            onload: function() {
                response = JSON.parse(this.responseText);
                if (response.success) {
                    Alloy.Globals.user.account = response.data.account;
                    Alloy.Globals.user.nickname = response.data.nickname;
                    Ti.API.debug(Alloy.Globals.user.account);
                    Ti.API.debug(Alloy.Globals.user.nickname);
                    Alloy.Globals.openMainWindow("user");
                } else {
                    $.message.color = "red";
                    $.message.text = response.data.errors.message[0];
                    $.message.setVisible(true);
                }
            },
            onerror: function(e) {
                Ti.API.error(e.message);
            },
            timeout: 3e3
        });
        var params = {
            account: $.account.value,
            password: Ti.Utils.md5HexDigest($.password.value)
        };
        xhr.open("POST", "http://www.barryblogs.com/index.php?feed=sitcon&action=login");
        xhr.send(params);
    }
    function register() {
        Alloy.Globals.openMainWindow("register");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "login";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.login = Ti.UI.createWindow({
        backgroundColor: "#4A4A4A",
        id: "login"
    });
    $.__views.login && $.addTopLevelView($.__views.login);
    $.__views.header = Ti.UI.createView({
        height: "20%",
        top: 0,
        backgroundColor: "#065efe",
        id: "header"
    });
    $.__views.login.add($.__views.header);
    $.__views.logo = Ti.UI.createImageView({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        id: "logo",
        image: "/images/logo.png"
    });
    $.__views.header.add($.__views.logo);
    $.__views.content = Ti.UI.createView({
        top: "20%",
        bottom: "20%",
        layout: "vertical",
        id: "content"
    });
    $.__views.login.add($.__views.content);
    $.__views.message = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "red",
        font: {
            fontSize: "28px"
        },
        id: "message"
    });
    $.__views.content.add($.__views.message);
    $.__views.__alloyId1 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId1"
    });
    $.__views.content.add($.__views.__alloyId1);
    $.__views.__alloyId2 = Ti.UI.createLabel({
        width: "20%",
        height: Ti.UI.SIZE,
        color: "white",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        text: L("account"),
        id: "__alloyId2"
    });
    $.__views.__alloyId1.add($.__views.__alloyId2);
    $.__views.account = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "account",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "請輸入帳號",
        keyboardType: Ti.UI.KEYBOARD_EMAIL
    });
    $.__views.__alloyId1.add($.__views.account);
    $.__views.__alloyId3 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId3"
    });
    $.__views.content.add($.__views.__alloyId3);
    $.__views.__alloyId4 = Ti.UI.createLabel({
        width: "20%",
        height: Ti.UI.SIZE,
        color: "white",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        text: "密碼：",
        id: "__alloyId4"
    });
    $.__views.__alloyId3.add($.__views.__alloyId4);
    $.__views.password = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "password",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "請輸入密碼",
        passwordMask: "true"
    });
    $.__views.__alloyId3.add($.__views.password);
    $.__views.__alloyId5 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId5"
    });
    $.__views.content.add($.__views.__alloyId5);
    $.__views.btn_register = Ti.UI.createButton({
        id: "btn_register",
        title: "註冊",
        width: "30%",
        height: Ti.UI.SIZE
    });
    $.__views.__alloyId5.add($.__views.btn_register);
    register ? $.__views.btn_register.addEventListener("click", register) : __defers["$.__views.btn_register!click!register"] = true;
    $.__views.btn_login = Ti.UI.createButton({
        id: "btn_login",
        title: "登入",
        width: "30%",
        height: Ti.UI.SIZE
    });
    $.__views.__alloyId5.add($.__views.btn_login);
    login ? $.__views.btn_login.addEventListener("click", login) : __defers["$.__views.btn_login!click!login"] = true;
    $.__views.footer = Ti.UI.createView({
        height: "10%",
        bottom: 0,
        backgroundColor: "#4A4A4A",
        id: "footer"
    });
    $.__views.login.add($.__views.footer);
    $.__views.copyright = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "white",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "copyright",
        text: "Copyright 2013 Barry"
    });
    $.__views.footer.add($.__views.copyright);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.login;
    $.account.value = "riceooks";
    $.password.value = "62500727";
    alert(L("account"));
    $.login.open();
    __defers["$.__views.btn_register!click!register"] && $.__views.btn_register.addEventListener("click", register);
    __defers["$.__views.btn_login!click!login"] && $.__views.btn_login.addEventListener("click", login);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;