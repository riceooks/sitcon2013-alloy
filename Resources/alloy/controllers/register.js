function Controller() {
    function submit() {
        message = "";
        0 == $.nickname.value.length && (message += "請輸入暱稱\n");
        6 > $.account.value.length && (message += "請輸入帳號\n");
        6 > $.password.value.length && (message += "密碼必須大於6個字元\n");
        (0 == $.verify.value.length || $.verify.value != $.password.value) && (message += 0 != $.verify.value.length ? "密碼與確認密碼不相同\n" : "請輸入確認密碼\n");
        if ("" == message) {
            var xhr = Ti.Network.createHTTPClient({
                onload: function() {
                    response = JSON.parse(this.responseText);
                    if (response.success) {
                        Alloy.Globals.user.account = response.data.account;
                        Alloy.Globals.user.nickname = response.data.nickname;
                        Alloy.Globals.openMainWindow("user");
                        controller.close();
                    }
                    message = response.data.errors.message[0];
                    dialog.message = message;
                    dialog.show();
                },
                onerror: function() {
                    message = "請檢查網路連線";
                    dialog.message = message;
                    dialog.show();
                },
                timeout: 8e3
            });
            var params = {
                account: $.account.value,
                password: $.password.value,
                nickname: $.nickname.value
            };
            xhr.open("POST", "http://www.barryblogs.com/index.php?feed=sitcon&action=register");
            xhr.send(params);
        } else {
            dialog.message = message;
            dialog.show();
        }
    }
    function back() {
        controller.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "register";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.register = Ti.UI.createWindow({
        backgroundColor: "#4A4A4A",
        id: "register"
    });
    $.__views.register && $.addTopLevelView($.__views.register);
    $.__views.header = Ti.UI.createView({
        height: "10%",
        top: 0,
        backgroundColor: "black",
        layout: "horizontal",
        id: "header"
    });
    $.__views.register.add($.__views.header);
    $.__views.home = Ti.UI.createButton({
        width: "30%",
        height: "100%",
        backgroundColor: "#EBEBEA",
        backgroundSelectedColor: "#F5C55E",
        right: "1%",
        id: "home",
        title: "首頁"
    });
    $.__views.header.add($.__views.home);
    back ? $.__views.home.addEventListener("click", back) : __defers["$.__views.home!click!back"] = true;
    $.__views.__alloyId6 = Ti.UI.createView({
        width: "69%",
        backgroundColor: "#EBEBEA",
        id: "__alloyId6"
    });
    $.__views.header.add($.__views.__alloyId6);
    $.__views.content = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "10%",
        bottom: "10%",
        layout: "vertical",
        id: "content"
    });
    $.__views.register.add($.__views.content);
    $.__views.title = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        font: {
            fontSize: "48px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "title",
        text: "註冊會員"
    });
    $.__views.content.add($.__views.title);
    $.__views.__alloyId7 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId7"
    });
    $.__views.content.add($.__views.__alloyId7);
    $.__views.__alloyId8 = Ti.UI.createLabel({
        width: "25%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false,
        text: "暱稱：",
        id: "__alloyId8"
    });
    $.__views.__alloyId7.add($.__views.__alloyId8);
    $.__views.nickname = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "nickname",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "請輸入暱稱",
        keyboardType: Ti.UI.KEYBOARD_EMAIL
    });
    $.__views.__alloyId7.add($.__views.nickname);
    $.__views.__alloyId9 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId9"
    });
    $.__views.content.add($.__views.__alloyId9);
    $.__views.__alloyId10 = Ti.UI.createLabel({
        width: "25%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false,
        text: "帳號：",
        id: "__alloyId10"
    });
    $.__views.__alloyId9.add($.__views.__alloyId10);
    $.__views.account = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "account",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "請輸入帳號",
        keyboardType: Ti.UI.KEYBOARD_EMAIL
    });
    $.__views.__alloyId9.add($.__views.account);
    $.__views.__alloyId11 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId11"
    });
    $.__views.content.add($.__views.__alloyId11);
    $.__views.__alloyId12 = Ti.UI.createLabel({
        width: "25%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false,
        text: "密碼：",
        id: "__alloyId12"
    });
    $.__views.__alloyId11.add($.__views.__alloyId12);
    $.__views.password = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "password",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "請輸入密碼",
        passwordMask: "true"
    });
    $.__views.__alloyId11.add($.__views.password);
    $.__views.__alloyId13 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId13"
    });
    $.__views.content.add($.__views.__alloyId13);
    $.__views.__alloyId14 = Ti.UI.createLabel({
        width: "25%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false,
        text: "確認密碼：",
        id: "__alloyId14"
    });
    $.__views.__alloyId13.add($.__views.__alloyId14);
    $.__views.verify = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "verify",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "請輸入密碼",
        passwordMask: "true"
    });
    $.__views.__alloyId13.add($.__views.verify);
    $.__views.__alloyId15 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId15"
    });
    $.__views.content.add($.__views.__alloyId15);
    $.__views.btn_submit = Ti.UI.createButton({
        id: "btn_submit",
        title: "送出",
        width: "30%",
        height: Ti.UI.SIZE
    });
    $.__views.__alloyId15.add($.__views.btn_submit);
    submit ? $.__views.btn_submit.addEventListener("click", submit) : __defers["$.__views.btn_submit!click!submit"] = true;
    $.__views.footer = Ti.UI.createView({
        height: "10%",
        bottom: 0,
        backgroundColor: "#4A4A4A",
        id: "footer"
    });
    $.__views.register.add($.__views.footer);
    $.__views.copyright = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "white",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "copyright",
        text: "Copyright 2013 Barry"
    });
    $.__views.footer.add($.__views.copyright);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var controller = $.register;
    var message = "";
    var dialog = Ti.UI.createAlertDialog({
        title: "提示訊息",
        buttonNames: [ "確定" ]
    });
    controller.open();
    __defers["$.__views.home!click!back"] && $.__views.home.addEventListener("click", back);
    __defers["$.__views.btn_submit!click!submit"] && $.__views.btn_submit.addEventListener("click", submit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;