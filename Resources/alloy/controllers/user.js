function Controller() {
    function update() {
        var xhr = Ti.Network.createHTTPClient({
            onload: function() {
                response = JSON.parse(this.responseText);
                if (response.success) {
                    dialog.message = "資料更新完成";
                    dialog.show();
                } else {
                    message = response.data.errors.message[0];
                    dialog.message = message;
                    dialog.show();
                }
            },
            onerror: function(e) {
                Ti.API.error(e.message);
            },
            timeout: 3e3
        });
        message = "";
        var params = {};
        0 == $.nickname.value.length ? message += "請輸入暱稱\n" : params.nickname = $.nickname.value;
        if (0 == $.verify.value.length || $.verify.value != $.password.value) {
            6 > $.password.value.length && (message += "密碼必須大於6個字元\n");
            0 != $.verify.value.length && (message += "密碼與確認密碼不相同\n");
            params.password = $.password.value;
        }
        "" == message;
        xhr.open("POST", "http://www.barryblogs.com/index.php?feed=sitcon&action=update");
        xhr.send(params);
    }
    function back() {
        controller.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "user";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.user = Ti.UI.createWindow({
        backgroundColor: "#4A4A4A",
        id: "user"
    });
    $.__views.user && $.addTopLevelView($.__views.user);
    $.__views.header = Ti.UI.createView({
        height: "10%",
        top: 0,
        backgroundColor: "black",
        layout: "horizontal",
        id: "header"
    });
    $.__views.user.add($.__views.header);
    $.__views.home = Ti.UI.createButton({
        width: "30%",
        height: "100%",
        backgroundColor: "#EBEBEA",
        backgroundSelectedColor: "#F5C55E",
        right: "1%",
        id: "home",
        title: "登出"
    });
    $.__views.header.add($.__views.home);
    back ? $.__views.home.addEventListener("click", back) : __defers["$.__views.home!click!back"] = true;
    $.__views.__alloyId16 = Ti.UI.createView({
        width: "69%",
        backgroundColor: "#EBEBEA",
        id: "__alloyId16"
    });
    $.__views.header.add($.__views.__alloyId16);
    $.__views.content = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "10%",
        bottom: "10%",
        layout: "vertical",
        id: "content"
    });
    $.__views.user.add($.__views.content);
    $.__views.title = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        font: {
            fontSize: "48px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "title",
        text: "會員資料"
    });
    $.__views.content.add($.__views.title);
    $.__views.__alloyId17 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId17"
    });
    $.__views.content.add($.__views.__alloyId17);
    $.__views.__alloyId18 = Ti.UI.createLabel({
        width: "25%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false,
        text: "暱稱：",
        id: "__alloyId18"
    });
    $.__views.__alloyId17.add($.__views.__alloyId18);
    $.__views.nickname = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "nickname",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "請輸入暱稱",
        keyboardType: Ti.UI.KEYBOARD_EMAIL
    });
    $.__views.__alloyId17.add($.__views.nickname);
    $.__views.__alloyId19 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId19"
    });
    $.__views.content.add($.__views.__alloyId19);
    $.__views.__alloyId20 = Ti.UI.createLabel({
        width: "25%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false,
        text: "帳號：",
        id: "__alloyId20"
    });
    $.__views.__alloyId19.add($.__views.__alloyId20);
    $.__views.account = Ti.UI.createLabel({
        width: "65%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "32px"
        },
        id: "account"
    });
    $.__views.__alloyId19.add($.__views.account);
    $.__views.__alloyId21 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId21"
    });
    $.__views.content.add($.__views.__alloyId21);
    $.__views.__alloyId22 = Ti.UI.createLabel({
        width: "25%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false,
        text: "密碼：",
        id: "__alloyId22"
    });
    $.__views.__alloyId21.add($.__views.__alloyId22);
    $.__views.password = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "password",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "若不更新請勿輸入",
        passwordMask: "true"
    });
    $.__views.__alloyId21.add($.__views.password);
    $.__views.__alloyId23 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId23"
    });
    $.__views.content.add($.__views.__alloyId23);
    $.__views.__alloyId24 = Ti.UI.createLabel({
        width: "25%",
        height: Ti.UI.SIZE,
        color: "#000",
        left: "5%",
        font: {
            fontSize: "25px"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        wordWrap: false,
        text: "確認密碼：",
        id: "__alloyId24"
    });
    $.__views.__alloyId23.add($.__views.__alloyId24);
    $.__views.verify = Ti.UI.createTextField({
        width: "65%",
        left: "5%",
        height: Ti.UI.SIZE,
        id: "verify",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText: "若不更新請勿輸入",
        passwordMask: "true"
    });
    $.__views.__alloyId23.add($.__views.verify);
    $.__views.__alloyId25 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId25"
    });
    $.__views.content.add($.__views.__alloyId25);
    $.__views.btn_update = Ti.UI.createButton({
        id: "btn_update",
        title: "更新",
        width: "30%",
        height: Ti.UI.SIZE
    });
    $.__views.__alloyId25.add($.__views.btn_update);
    update ? $.__views.btn_update.addEventListener("click", update) : __defers["$.__views.btn_update!click!update"] = true;
    $.__views.footer = Ti.UI.createView({
        height: "10%",
        bottom: 0,
        backgroundColor: "#4A4A4A",
        id: "footer"
    });
    $.__views.user.add($.__views.footer);
    $.__views.copyright = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "white",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "copyright",
        text: "Copyright 2013 Barry"
    });
    $.__views.footer.add($.__views.copyright);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var controller = $.user;
    var message = "";
    var dialog = Ti.UI.createAlertDialog({
        title: "提示訊息",
        buttonNames: [ "確定" ]
    });
    $.nickname.value = Alloy.Globals.user.nickname;
    $.account.text = Alloy.Globals.user.account;
    controller.open();
    __defers["$.__views.home!click!back"] && $.__views.home.addEventListener("click", back);
    __defers["$.__views.btn_update!click!update"] && $.__views.btn_update.addEventListener("click", update);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;